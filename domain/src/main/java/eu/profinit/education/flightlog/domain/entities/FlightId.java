package eu.profinit.education.flightlog.domain.entities;

import lombok.*;
import org.springframework.util.Assert;

import java.io.Serializable;

@Value
@AllArgsConstructor
@EqualsAndHashCode
public class FlightId implements Serializable {

    Long id;

    public static FlightId of(Long id) {
        Assert.notNull(id, "Flight ID cannot be null");
        return new FlightId(id);
    }
}