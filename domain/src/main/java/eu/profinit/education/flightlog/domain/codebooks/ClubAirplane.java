package eu.profinit.education.flightlog.domain.codebooks;

import eu.profinit.education.flightlog.domain.JpaConstants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

import static eu.profinit.education.flightlog.domain.JpaConstants.Tables.CLUB_AIRPLANE;
import static lombok.AccessLevel.PACKAGE;

@Entity
@Getter
@Setter
@ToString
@Table(name = CLUB_AIRPLANE)
@NoArgsConstructor()
public class ClubAirplane {

    public ClubAirplane(Long id, String immatriculation, boolean archived) {
        this.id = id;
        this.immatriculation = immatriculation;
        this.archived = archived;
        AirplaneType airplaneType = new AirplaneType();
        airplaneType.setType("manual_type");
        airplaneType.setId(0L);
        airplaneType.setMaxCapacity(2);
        this.type = airplaneType;
    }

    @Id
    private Long id;

    @Column(unique = true)
    private String immatriculation;

    @ManyToOne
    @JoinColumn(name = JpaConstants.Columns.TYPE_ID)
    private AirplaneType type;

    private boolean archived;


}
