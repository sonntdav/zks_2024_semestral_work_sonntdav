package eu.profinit.education.flightlog.service;

import eu.profinit.education.flightlog.domain.codebooks.AirplaneType;
import eu.profinit.education.flightlog.domain.codebooks.ClubAirplane;
import eu.profinit.education.flightlog.domain.entities.Airplane;
import eu.profinit.education.flightlog.domain.repositories.ClubAirplaneRepository;
import eu.profinit.education.flightlog.to.AirplaneTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AirplaneServiceTest {
    @Mock
    private ClubAirplaneRepository clubAirplaneRepository;

    private AirplaneService testSubject;

    @BeforeEach
    public void setUp() {
        testSubject = new AirplaneServiceImpl(clubAirplaneRepository);
    }

    @Test
    void shouldReturnAllAirplanes(){
        Long id = 0L;
        String immatr = "test_immatr";
        ClubAirplane airplane = new ClubAirplane(id,immatr,true);
        when(clubAirplaneRepository.findAll(Sort.by("immatriculation"))).thenReturn(List.of(airplane));

        List<AirplaneTo> result = testSubject.getClubAirplanes();
        List<AirplaneTo> expectedResult = List.of(AirplaneTo.fromEntity(airplane));
        assertEquals(expectedResult,result);
    }
}
