package eu.profinit.education.flightlog.convertors;

import eu.profinit.education.flightlog.exceptions.ValidationException;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class ExceptionConvertor extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
        if (o.equals("null")){
            return null;
        } else if (o.equals("ValidationException")) {
            return new ValidationException("test_exception");
        }
        return o;
    }
}
