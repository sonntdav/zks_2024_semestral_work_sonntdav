package eu.profinit.education.flightlog.convertors;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

import java.time.LocalDateTime;

public class TimeConvertor extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
        if (o.equals("now")){
            return LocalDateTime.now();
        } else if (o.equals("past")){
            return LocalDateTime.of(2022,8,10,1,1);
        } else if (o.equals("future")){
            return LocalDateTime.of(2025,8,10,1,1);
        } else if (o.equals("null")){
            return null;
        }
        return o;
    }
}
