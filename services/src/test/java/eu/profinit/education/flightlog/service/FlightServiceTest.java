package eu.profinit.education.flightlog.service;

import eu.profinit.education.flightlog.common.Clock;
import eu.profinit.education.flightlog.convertors.ExceptionConvertor;
import eu.profinit.education.flightlog.convertors.StringConvertor;
import eu.profinit.education.flightlog.convertors.TimeConvertor;
import eu.profinit.education.flightlog.convertors.TypeConvertor;
import eu.profinit.education.flightlog.domain.entities.*;
import eu.profinit.education.flightlog.domain.fields.Task;
import eu.profinit.education.flightlog.domain.repositories.ClubAirplaneRepository;
import eu.profinit.education.flightlog.domain.repositories.FlightRepository;
import eu.profinit.education.flightlog.exceptions.ValidationException;
import eu.profinit.education.flightlog.to.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FlightServiceTest {
    @Mock
    private FlightRepository flightRepository;

    @Mock
    private ClubAirplaneRepository clubAirplaneRepository;

    @Mock
    private Clock clock;

    @Mock
    private PersonService personService;

    @Mock
    private FlightService testSubject;

    @BeforeEach
    public void setUp() {
        testSubject = new FlightServiceImpl(flightRepository,clubAirplaneRepository,clock,personService);
    }

    @Test
    void shouldThrowError(){
        FlightTakeoffTo flightTakeoffTo = FlightTakeoffTo.builder().build();
        assertThrows(ValidationException.class,() -> {
            testSubject.takeoff(flightTakeoffTo);
        });
    }

    @Test
    void shouldSaveFlightWithGlider(){
        //setup
        LocalDateTime now = LocalDateTime.now();
        PersonTo pilotTo = getPilot();
        PersonTo copilotTo = getCopilot();

        Airplane towplane = Airplane.guestAirplane("test_imatr_tow","test_type_tow");
        Airplane glider = Airplane.guestAirplane("test_imatr_glider","test_type_glider");
        AirplaneWithCrewTo airplaneWithCrewTo = AirplaneWithCrewTo.builder().pilot(pilotTo).airplane(AirplaneTo.fromEntity(towplane)).build();
        AirplaneWithCrewTo gliderWithCrewTo = AirplaneWithCrewTo.builder().pilot(copilotTo).airplane(AirplaneTo.fromEntity(glider)).build();

        FlightTakeoffTo input = FlightTakeoffTo.builder().takeoffTime(now).towplane(airplaneWithCrewTo).glider(gliderWithCrewTo).task("test_task").build();


        //setup 2
        Person pilot = new Person(Person.Type.GUEST,pilotTo.getFirstName(),pilotTo.getLastName(),null);
        Person copilot = new Person(Person.Type.GUEST,pilotTo.getFirstName(),pilotTo.getLastName(),null);


        Flight gliderFlight = new Flight(Flight.Type.GLIDER, Task.of(input.getTask()), input.getTakeoffTime(), glider, copilot, null, input.getGlider().getNote());
        //mock
        when(personService.getExistingOrCreatePerson(pilotTo)).thenReturn(pilot);
        when(personService.getExistingOrCreatePerson(copilotTo)).thenReturn(copilot);
        when(personService.getExistingOrCreatePerson(null)).thenReturn(null);
        when(flightRepository.save(any())).thenReturn(gliderFlight);

        testSubject.takeoff(input);
        //verify
        verify(personService,times(1)).getExistingOrCreatePerson(pilotTo);
        verify(personService,times(1)).getExistingOrCreatePerson(copilotTo);
    }

    @Test
    void shouldLandCorrectly(){
        FlightId flightId = new FlightId(0L);
        Flight flight = new Flight(Flight.Type.GLIDER,null,LocalDateTime.now(),Airplane.guestAirplane("test_imr","test_type"),null,null,"note");
        LocalDateTime landingTime = LocalDateTime.of(2025,8,10,1,1);
        //mock
        when(clock.now()).thenReturn(landingTime);
        when(flightRepository.findById(flightId.getId())).thenReturn(Optional.of(flight));
        testSubject.land(flightId,null);
        verify(flightRepository,times(1)).save(flight);
    }

    @Test
    void shouldResultInExceptionCannotLandBeforeTakeOff(){
        FlightId flightId = new FlightId(0L);
        Flight flight = new Flight(Flight.Type.GLIDER,null,LocalDateTime.now(),Airplane.guestAirplane("test_imr","test_type"),null,null,"note");
        LocalDateTime landingTime = LocalDateTime.of(2022,8,10,1,1);
        //mock
        when(clock.now()).thenReturn(landingTime);
        when(flightRepository.findById(flightId.getId())).thenReturn(Optional.of(flight));
        assertThrows(ValidationException.class,() -> {
            testSubject.land(flightId,null);
        });
    }
    @Test
    void shouldResultInExceptionFlightHasAlreadyLanded(){
        FlightId flightId = new FlightId(0L);
        Flight flight = new Flight(Flight.Type.GLIDER,null,LocalDateTime.now(),Airplane.guestAirplane("test_imr","test_type"),null,null,"note");
        LocalDateTime landingTime = LocalDateTime.of(2024,8,10,1,1);
        flight.setLandingTime(landingTime);
        //mock
        when(clock.now()).thenReturn(landingTime);
        when(flightRepository.findById(flightId.getId())).thenReturn(Optional.of(flight));
        assertThrows(ValidationException.class,() -> {
            testSubject.land(flightId,null);
        });
    }

    @Test
    void shouldReturnFlightsForReport(){
        PersonTo pilotTo = getPilot();
        LocalDateTime now = LocalDateTime.now();
        Airplane towPlane = Airplane.guestAirplane("tow_immatr","tow_type");
        Airplane gliderPlane = Airplane.guestAirplane("glider_immatr","glider_immatr");

        Person pilot = new Person(Person.Type.GUEST,pilotTo.getFirstName(),pilotTo.getLastName(),null);
        Person copilot = new Person(Person.Type.GUEST,pilotTo.getFirstName(),pilotTo.getLastName(),null);

        Task tow_task = new Task("tow_task");
        Task glider_task = new Task("glider_task");
        Flight tow_flight = new Flight(Flight.Type.TOWPLANE,tow_task,now,towPlane,pilot,null,"tow_note");
        Flight glider_flight = new Flight(Flight.Type.GLIDER,glider_task,now,gliderPlane,copilot,null,"glider_note");
        tow_flight.setGliderFlight(glider_flight);
        FlightTuppleTo expectedResult = FlightTuppleTo.builder().towplane(FlightTo.fromEntity(tow_flight)).glider(FlightTo.fromEntity(glider_flight)).build();
        when(flightRepository.findAllByFlightTypeOrderByTakeoffTimeAscIdAsc(Flight.Type.TOWPLANE)).thenReturn(List.of(tow_flight));

        List<FlightTuppleTo> result = testSubject.getFlightsForReport();
        assertEquals(List.of(expectedResult),result);

    }

    @Test
    void shouldGetFlightInTheAir(){
        PersonTo pilotTo = getPilot();
        LocalDateTime now = LocalDateTime.now();
        Airplane towPlane = Airplane.guestAirplane("tow_immatr","tow_type");
        Person pilot = new Person(Person.Type.GUEST,pilotTo.getFirstName(),pilotTo.getLastName(),null);
        Task tow_task = new Task("tow_task");
        Flight tow_flight = new Flight(Flight.Type.TOWPLANE,tow_task,now,towPlane,pilot,null,"tow_note");


        when(flightRepository.findAllByLandingTimeNullOrderByTakeoffTimeAscIdAsc()).thenReturn(List.of(tow_flight));

        List<FlightTo> result = testSubject.getFlightsInTheAir();
        assertEquals(List.of(FlightTo.fromEntity(tow_flight)),result);
    }

    @Test
    void shouldThrowExceptionNoTowplane(){
        FlightTakeoffTo flight = FlightTakeoffTo.builder().build();
        assertThrows(ValidationException.class,() -> {
            testSubject.takeoff(flight);
        });
    }
    private PersonTo getPilot(){
        return PersonTo.builder()
            .firstName("Jan")
            .lastName("Novák")
            .address(AddressTo.builder()
                .street("Tychonova 2")
                .city("Praha 6")
                .postalCode("16000")
                .build())
            .build();
    }
    private PersonTo getCopilot(){
        return PersonTo.builder()
            .firstName("Jan_cop")
            .lastName("Novák_cop")
            .address(AddressTo.builder()
                .street("Tychonova 2_cop")
                .city("Praha 6_cop")
                .postalCode("16000_cop")
                .build())
            .build();
    }


    @ParameterizedTest
    @CsvFileSource(resources = "/flightLog-output.csv", delimiter = ',', numLinesToSkip = 1)
    public void myTest(
        @ConvertWith(TypeConvertor.class) Person.Type towPlanePilotType,
        @ConvertWith(StringConvertor.class) String towPlanePilotName,
        @ConvertWith(StringConvertor.class) String towPlanePilotLastName,
        @ConvertWith(TypeConvertor.class) Person.Type towPlaneCopilotType,
        @ConvertWith(StringConvertor.class) String towPlaneCopilotName,
        @ConvertWith(StringConvertor.class) String towPlaneCopilotLastName,
        @ConvertWith(TypeConvertor.class) Person.Type gliderPilotType,
        @ConvertWith(StringConvertor.class) String gliderPilotName,
        @ConvertWith(StringConvertor.class) String gliderPilotLastName,
        @ConvertWith(TypeConvertor.class) Person.Type gliderCopilotType,
        @ConvertWith(StringConvertor.class) String gliderCopilotName,
        @ConvertWith(StringConvertor.class) String gliderCopilotLastName,
        @ConvertWith(StringConvertor.class) String towPlaneImmatriculation,
        @ConvertWith(StringConvertor.class) String towPlaneType,
        @ConvertWith(StringConvertor.class) String gliderImmatriculation,
        @ConvertWith(StringConvertor.class) String gliderType,
        @ConvertWith(StringConvertor.class) String towPlaneNote,
        @ConvertWith(StringConvertor.class) String gliderNote,
        @ConvertWith(TimeConvertor.class) LocalDateTime time,
        @ConvertWith(StringConvertor.class) String flightNote,
        @ConvertWith(ExceptionConvertor.class) Exception expectedResult
    ) {
        // init data
        Person towPlanePilot = new Person(towPlanePilotType, towPlanePilotName, towPlanePilotLastName, getAddress());
        Person towPlaneCopilot = new Person(towPlaneCopilotType, towPlaneCopilotName, towPlaneCopilotLastName, getAddress());
        Person gliderPilot = new Person(gliderPilotType, gliderPilotName, gliderPilotLastName, getAddress());
        Person gliderCopilot = new Person(gliderCopilotType, gliderCopilotName, gliderCopilotLastName, getAddress());

        PersonTo towPlanePilotTo = PersonTo.fromEntity(towPlanePilot);
        PersonTo towPlaneCopilotTo = PersonTo.fromEntity(towPlaneCopilot);
        PersonTo gliderPilotTo = PersonTo.fromEntity(gliderPilot);
        PersonTo gliderCopilotTo = PersonTo.fromEntity(gliderCopilot);

        Airplane towPlane = Airplane.guestAirplane(towPlaneImmatriculation,towPlaneType);
        Airplane glider = Airplane.guestAirplane(gliderImmatriculation,gliderType);

        AirplaneTo towPlaneTo = AirplaneTo.fromEntity(towPlane);
        AirplaneTo gliderTo = AirplaneTo.fromEntity(glider);

        AirplaneWithCrewTo towPlaneWithCrewTo = AirplaneWithCrewTo.builder()
            .airplane(towPlaneTo)
            .pilot(towPlanePilotTo)
            .copilot(towPlaneCopilotTo)
            .note(towPlaneNote)
            .build();

        AirplaneWithCrewTo gliderWithCrewTo = AirplaneWithCrewTo.builder()
            .airplane(gliderTo)
            .pilot(gliderPilotTo)
            .copilot(gliderCopilotTo)
            .note(gliderNote)
            .build();

        FlightTakeoffTo flightTakeoffTo = FlightTakeoffTo.builder()
            .towplane(towPlaneWithCrewTo)
            .glider(gliderWithCrewTo)
            .takeoffTime(time)
            .task(flightNote)
            .build();

        if (expectedResult == null){
            assertDoesNotThrow(() -> testSubject.takeoff(flightTakeoffTo));
        } else if (Objects.equals(expectedResult.getMessage(), "test_exception")) {
            assertThrows(expectedResult.getClass(),() -> testSubject.takeoff(flightTakeoffTo));
        }

    }
    private Address getAddress(){
        return null;
    }
}
