package eu.profinit.education.flightlog.convertors;

import eu.profinit.education.flightlog.domain.entities.Person;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class TypeConvertor extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
        if (o.equals("GUEST")){
            return Person.Type.GUEST;
        } else if (o.equals("CLUB_MEMBER")) {
            return Person.Type.CLUB_MEMBER;
        }
        return o;
    }
}
